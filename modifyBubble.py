
import binascii

newFile = open("newBubbleBobble.nsf", "wb")

byteArray = []
with open("BubbleBobble.nsf", "rb") as f:
    byte = f.read(1)
    while byte:
        # Do stuff with byte.
#        newFile.write(byte)
        byteArray.append(byte)
        byte = f.read(1)
#        print ord(byte)

#print len(byteArray)
#print binascii.hexlify(byteArray[0x200])
#print binascii.hexlify(byteArray[0x201])

noteTable = { "Eb6":0x5a,
        "D6":0x5f,
        "C#6":0x64,
        "C6":0x6b,
        "Bb5":0x78,
        "A5":0x7f,
        "Ab5":0x87,
        "G5":0x8f,
        "F#5":0x96,
        "F5":0xa0,
        "E5":0xa9,
        "Eb5":0xb4,
        "D5":0xbe,
        "C#5":0xc8,
        "C5":0xd6,
        "B4":0xe4,
        "Bb4":0xf0
        }


offsetAddr = 0x8417
startOfMusic = 0x495
endOfMusic = 0x2080
i = 0
while i < len(byteArray):

    byteCount = offsetAddr - 0x8417
    # Navigate to the song table and stop.
    if i == startOfMusic:
#        newFile.write("a")
#        print "Music Table: " + binascii.hexlify(byteArray[i])

        # The style of a song table is as follows:
        # Index of the song is stored into the song table at 0x8415.
        #    An index for a song is stored as song address - 1.
        # The location of the song begins with a JSR to init routines.
        # The first init is 0x8017 which I did not reverse.
        # Here we only will init Pulse2 with subroutine 0x8205.
        # The two bytes after the init routine are a pointer to the channels data.
        # After init routines and data there is 0x60 or RTS.
        # NOTE: All addresses are little endian.
        # Insert a song index into the table which points exactly right behind it.
        newFile.write(bytearray([0x16]))
        newFile.write(bytearray([0x84]))
        offsetAddr += 2
        i += 2
        # Insert the jump to unknown init.
        newFile.write(bytearray([0x20]))
        newFile.write(bytearray([0x17]))
        newFile.write(bytearray([0x80]))
        offsetAddr += 3
        i += 3
        # Insert init to pulse 2 channel.
        newFile.write(bytearray([0x20]))
        newFile.write(bytearray([0x05]))
        newFile.write(bytearray([0x82]))
        offsetAddr += 3
        i += 3
        # Insert pointer to channel data.
#        print hex(offsetAddr + 3)
#        lowNibble = ((offsetAddr + 3) & 0xFF)
#        highNibble = (((offsetAddr + 3) >> 8) & 0xFF)
#        newFile.write(bytearray([lowNibble]))
#        newFile.write(bytearray([highNibble]))
        newFile.write(bytearray([0x20]))
        newFile.write(bytearray([0x84]))
        offsetAddr += 2
        i += 2
        # Insert RTS.
        newFile.write(bytearray([0x60]))
        offsetAddr += 1
        i += 1
        # This marks the beginning of the channel data.
        # Insert global song data.
        newFile.write(bytearray([0x00])) # Disable?
        newFile.write(bytearray([0xfe])) # Not sure.
        newFile.write(bytearray([0x05])) # Song Speed.
        offsetAddr += 3
        i += 3
        # Square wave characteristics.
        newFile.write(bytearray([0x00])) # Disable?
        newFile.write(bytearray([0x9c])) # Duty cycle and volume.
        newFile.write(bytearray([0x00])) # Sweep register.
        offsetAddr += 3
        i += 3
        # Insert three note addresses.
        lowNibble = ((offsetAddr + 4) & 0xFF)
        highNibble = (((offsetAddr + 4) >> 8) & 0xFF)
        newFile.write(bytearray([lowNibble]))
        newFile.write(bytearray([highNibble]))
        offsetAddr += 2
        i += 2
        lowNibble = ((offsetAddr + 4) & 0xFF)
        highNibble = (((offsetAddr + 4) >> 8) & 0xFF)
        newFile.write(bytearray([lowNibble]))
        newFile.write(bytearray([highNibble]))
        offsetAddr += 2
        i += 2
        lowNibble = ((offsetAddr + 4) & 0xFF)
        highNibble = (((offsetAddr + 4) >> 8) & 0xFF)
        newFile.write(bytearray([lowNibble]))
        newFile.write(bytearray([highNibble]))
        offsetAddr += 2
        i += 2
        newFile.write(bytearray([noteTable["Eb6"]]))
        newFile.write(bytearray([0x30]))
        offsetAddr += 2
        i += 2
        newFile.write(bytearray([noteTable["D6"]]))
        newFile.write(bytearray([0x30]))
        offsetAddr += 2
        i += 2
        newFile.write(bytearray([noteTable["C#6"]]))
        newFile.write(bytearray([0x30]))
        offsetAddr += 2
        i += 2
        newFile.write(bytearray([noteTable["C6"]]))
        newFile.write(bytearray([0x30]))
        offsetAddr += 2
        i += 2
        newFile.write(bytearray([noteTable["Bb5"]]))
        newFile.write(bytearray([0x30]))
        offsetAddr += 2
        i += 2
        newFile.write(bytearray([noteTable["A5"]]))
        newFile.write(bytearray([0x30]))
        offsetAddr += 2
        i += 2


#    if (i >= (startOfMusic + byteCount) and i <= (endOfMusic)):
#        newFile.write(bytearray([0xff]))

    else:
        newFile.write(byteArray[i])
        i += 1


print byteCount
print (hex(byteCount + 0x496))
#print (hex(0x1968 - byteCount))
