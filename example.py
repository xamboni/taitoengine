
import midi

midi_table = { 'A5':midi.A_5,
  'Ab5':midi.Ab_5,
  'Bb5':midi.Bb_5,
  'Bb4':midi.Bb_4,
  'C5':midi.C_5,
  'D5':midi.D_5,
  'Eb5':midi.Eb_5,
  'F5':midi.F_5,
  'G5':midi.G_5,
  'A6':midi.A_6,
  'B6':midi.B_6,
  'C6':midi.C_6,
  'D6':midi.D_6,
  'Eb6':midi.Eb_6,
}

divider = 1

tick_table = { #'0x0c Not sure of length.':(25 / divider),
               'Sixteenth':(50 / divider),
               'Eighth':(100 / divider),
               #'0x0a Not sure of length.':(150 / divider),
               'Quarter':(200 / divider),
               'Dotted Quarter':(300 / divider),
               'Half':(400 / divider),
               'Dotted Half':(600 / divider),
               'Whole':(800 / divider),
               'Dotted Whole':(1200 / divider),
               '0x12 Not sure of length.':(1600 / divider),
}

# Instantiate a MIDI Pattern (contains a list of tracks)
pattern = midi.Pattern()
# Instantiate a MIDI Track (contains a list of MIDI events)
track = midi.Track()
# Append the track to the pattern
pattern.append(track)

notesFile = open("notes.txt", "r")
noteLines = notesFile.readlines()

for noteLine in noteLines:

    note = noteLine.split(":")

    # Instantiate a MIDI note on event, append it to the track
    if note[0] == "Rest.":
        continue
    on = midi.NoteOnEvent(tick=0, velocity=40, pitch=midi_table[note[0]])
    track.append(on)

    try:
        # Instantiate a MIDI note off event, append it to the track
        tickValue = int(note[2], 16) * 50
        off = midi.NoteOffEvent(tick=tickValue, pitch=midi_table[note[0]])
#        off = midi.NoteOffEvent(tick=tick_table[note[1]], pitch=midi_table[note[0]])
    except:
        off = midi.NoteOffEvent(tick=200, pitch=midi_table[note[0]])
        
    track.append(off)

# Add the end of track event, append it to the track
eot = midi.EndOfTrackEvent(tick=1)
track.append(eot)
# Print out the pattern
print pattern
# Save the pattern to disk
midi.write_midifile("example.mid", pattern)
