
# File:    modifyBubble.py
# Author:  Xamboni
# Date:    05/14/19
# Purpose: Take in BubbleBobble nsf and modify.

import binascii

###### Globals ######

loadAddress = 0x8000
headerSize = 0x80
musicTable = 0x8415
musicEntries = 36

PressingForward = 1
PickUpThePace = 2

bubbleBobbleTitle = ["TheQuestBegins", "PressingForward", "PickUpThePace",
                     "Title", "ThankYou", "GameOver", 
                     "Invincible", "TheyreHere", "Sfx_00", 
                     "HurryUp", "Sfx_01", "Sfx_02",
                     "Sfx_03", "Sfx_04", "Sfx_05", 
                     "Sfx_06", "Sfx_07", "Sfx_08", 
                     "Sfx_09", "Sfx_0a", "Sfx_0b", 
                     "Sfx_0c", "Sfx_0d", "Sfx_0e", 
                     "Sfx_0f", "Sfx_10", "Sfx_11", 
                     "TheFinalEncounter", "TheBadEnd", "StaffRoll",
                     "GoodEnding", "Victory", "Sfx_12",
                     "SecretTreasure", "Sfx_13", "Sfx_14",
                     ]

beatsPerMinuteTable = { 1:(900/1),
                        2:(900/2),
                        3:(900/3),
                        4:(900/4),
                        5:(900/5),
                        6:(900/6),
                        7:(900/7),
                        8:(900/8),
                        9:(900/9)
                       }

durationTable = { 0x02:"Eighth",
                  0x06:"Quarter",
                  0x00:"Dotted Quarter",
                  0x04:"Dotted Half",
                  0x0c:"0x0c Not sure of length.",
                  0x0a:"0x0a Not sure of length.",
                  0x12:"0x12 Not sure of length.",
                  0x08:"0x08 Not sure of length."
                }

durationTable4 = [ 0x03,
                   0x01,
                   0x06,
                   0x02,
                   0x09,
                   0x0c,
                   0x04,
                   0x12,
                   0x18,
                   0x08,
                   0x24,
                   0x30,
                   0x10,
                   0x48,
                   0x3c,
                   0x1e
                 ]

def createFrequency(t):
    f = 1789773 / (16 * (t + 1))
    return f

# Taito engine stores frequency as t+1 in ROM.
# NOTE: Notes that are not in key of Bb are not correct in this table.
noteTable = { 0x5a:"Eb6", #
              0x5f:"D6", # 1177
              0x64:"C#6", # 1046
              0x6b:"C6",   # 987
              0x78:"Bb5",  # 932
              0x7f:"A5",  # 880
              0x87:"Ab5",   # 830
              0x8f:"G5",   # 782
              0x96:"F#5",   # 740
              0xa0:"F5",  # 698
              0xa9:"E5",   # 659
              0xb4:"Eb5",   # 622
              0xbe:"D5",  # 587
              0xc8:"C#5",  # 554
              0xd6:"C5",  # 523
              0xe4:"B4",  # 493
              0xf0:"Bb4",  # 466
            }

# Unknown init?
# Init square wave 1.
# Init square wave 2.
# Init triangle wave.
# Init noise channel.
initRoutines = { 0x8017:"UnknownInit", 
                 0x81fe:"InitSquare1", 
                 0x8205:"InitSquare2", 
                 0x820c:"InitTriangle", 
                 0x8213:"InitNoise" }

# For debug.
notes = []

notesFile = open("notes.txt", "w")

###### Globals ######

def getPointer(bubbleBobbleFile, offset):

    # Move the file pointer to the offset within file given.
    bubbleBobbleFile.seek(offset - loadAddress + headerSize)

    lowNibble = bubbleBobbleFile.read(1)
    highNibble = bubbleBobbleFile.read(1)
    lowNibble = binascii.hexlify(lowNibble).zfill(2)
    highNibble = binascii.hexlify(highNibble).zfill(2)
    address = "0x" + highNibble + lowNibble
    return address

def getNote(bubbleBobbleFile, offset, dutyEnvelopeByte, sweepByte):

    currentPos = bubbleBobbleFile.tell()

    # Move the file pointer to the offset within file given.
    bubbleBobbleFile.seek(offset - loadAddress + headerSize)

    lowNibble = ord(bubbleBobbleFile.read(1))
    while lowNibble != 0x00:
        highNibble = ord(bubbleBobbleFile.read(1))
    
        # All 8 bits of first byte and first 3 of second represent frequency.
        # LLLL Lhhh | llll llll.
        noteDelay = ((highNibble & 0x07) << 8) | lowNibble

        if noteDelay <= 8:
             print "Rest."
        else:
            noteFrequency = createFrequency(noteDelay - 1) # Taito engine adds one in for optimization.
            print "Note: " + noteTable[noteDelay] + " Note frequency: " + str(noteFrequency) + ". Delay: " + hex(noteDelay)
    
        noteDuration = (highNibble & 0xF8) >> 3
        noteDuration4 = (highNibble & 0xF8) >> 4
        print "Note duration: " + durationTable[noteDuration] + " note."
        if noteDelay <= 8:
            notes.append( ("Rest.", durationTable[noteDuration]) )
        else:
            notes.append( (noteTable[noteDelay], durationTable[noteDuration], str(durationTable4[noteDuration4]), binascii.hexlify(dutyEnvelopeByte), binascii.hexlify(sweepByte)) )
            
        lowNibble = ord(bubbleBobbleFile.read(1))

    # Return the file back to normal.
    bubbleBobbleFile.seek(currentPos)


def printSongList(bubbleBobbleFile):

    # This would be better as a ghidra script.
    # Just use a global table for title at the moment.
    for i in xrange(0, musicEntries):
        address = getPointer(bubbleBobbleFile, musicTable + (2 * i))
        print( bubbleBobbleTitle[i] + ": " + address )

def analyzeInitData(bubbleBobbleFile, songIndex):
    print "Analyzing song " + bubbleBobbleTitle[songIndex] + "."
    initData = {}

    address = getPointer(bubbleBobbleFile, musicTable + (2 * songIndex))
    songDataIndex = int(address, 16)
    print "Song Data Pointer: " + address

    # Navigate to the pointer returned.
    bubbleBobbleFile.seek(songDataIndex - loadAddress + headerSize)

#NOTE: I am assuming 0x60 is a halt byte!
    # Unfortunately I havent figured out what the first bytes in the pointer mean.
    # For now just scroll until the jmp 8017. That routine is an init of some sort.
    # Edit: It seems as though all song addresses need one added to them.
    nextByte = int(binascii.hexlify(bubbleBobbleFile.read(1)), 16)
    songInitIndex = songDataIndex + 1
    while nextByte != 0x20:
        nextByte = int(binascii.hexlify(bubbleBobbleFile.read(1)), 16)
        songInitIndex += 1

    address = getPointer(bubbleBobbleFile, songInitIndex)
    songInitIndex += 2
    initPointer = int(address, 16)
    # If we don't find the unknown init print error and return.
    if initRoutines[initPointer] != "UnknownInit":
        print( "Error! Found a bad init starter address: " + address )
        return

    # Check for halt byte.
    nextByte = int(binascii.hexlify(bubbleBobbleFile.read(1)), 16)
    songInitIndex += 1
    while nextByte != 0x60:
        if nextByte == 0x20:
            address = getPointer(bubbleBobbleFile, songInitIndex)
            songInitIndex += 2
            initPointer = int(address, 16)
            print "InitPointer: " + hex(initPointer)
            if initPointer in initRoutines:
                print "Found: " + initRoutines[initPointer]
                dataAddress = getPointer(bubbleBobbleFile, songInitIndex)
                print "DataAddress: " + dataAddress
                initData[initPointer] = int(dataAddress, 16)
                songInitIndex += 2

            else:
                print( "Error! Found a bad init routine address: " + address )
                return

        else:
            print "Byte: " + nextByte

        nextByte = int(binascii.hexlify(bubbleBobbleFile.read(1)), 16)
        songInitIndex += 1    

    print "Located halt byte for " + bubbleBobbleTitle[songIndex] + "."
    return initData

def analyzeSquareOne(bubbleBobbleFile, squareOneDataOffset):
    # Navigate to the note data given.
    bubbleBobbleFile.seek(squareOneDataOffset - loadAddress + headerSize)

    # There are three bytes preceeding note pointers.
    disableChannel = bubbleBobbleFile.read(1)
    unknownFE = bubbleBobbleFile.read(1)
    songSpeed = bubbleBobbleFile.read(1)
    songSpeedStr = binascii.hexlify(songSpeed)

    print "Channel disable: " + binascii.hexlify(disableChannel) + "."
    print "Unknown FE: " + binascii.hexlify(unknownFE) + "."
    print "Song Speed: " + songSpeedStr + " or " + str(beatsPerMinuteTable[int(songSpeedStr, 16)]) + "bpm."

    # Begin parsing notes.
    # Three bytes to setup each note if needed.
    # Disable byte.
    # NOTE: right now I cannot tell how they know the next byte is a setup sequence or note address.
    # A really easy test is to see if they next byte is non-zero but how does this work if the note address ends in zero?
    # EDIT: I have to just work on this assumption.
    safety = 1
    while True:
        disableByte = bubbleBobbleFile.read(1)
        if int(binascii.hexlify(disableByte), 16) != 0:
            # Test to for the stop byte which is 0xff.
            print "Found a pointer that does not require init."
            lowNibble = disableByte
            highNibble = bubbleBobbleFile.read(1)
            lowNibble = binascii.hexlify(lowNibble).zfill(2)
            highNibble = binascii.hexlify(highNibble).zfill(2)
            noteData = "0x" + highNibble + lowNibble
            print "Note Data: " + noteData

        else:
            print "Note Disable Byte: " + binascii.hexlify(disableByte)
            dutyEnvelopeByte = bubbleBobbleFile.read(1)
            if int(binascii.hexlify(dutyEnvelopeByte), 16) == 0xff:
                print "End of notes."
                lowNibble = bubbleBobbleFile.read(1)
                highNibble = bubbleBobbleFile.read(1)
                lowNibble = binascii.hexlify(lowNibble).zfill(2)
                highNibble = binascii.hexlify(highNibble).zfill(2)
                contData = "0x" + highNibble + lowNibble
                print "Continuation Point: " + contData
                break
            print "DutyEnvelope Byte: " + binascii.hexlify(dutyEnvelopeByte)
            sweepByte = bubbleBobbleFile.read(1)
            print "Sweep Byte: " + binascii.hexlify(sweepByte)
            lowNibble = bubbleBobbleFile.read(1)
            highNibble = bubbleBobbleFile.read(1)
            lowNibble = binascii.hexlify(lowNibble).zfill(2)
            highNibble = binascii.hexlify(highNibble).zfill(2)
            noteData = "0x" + highNibble + lowNibble
            print "Note Data: " + noteData
        getNote(bubbleBobbleFile, int(noteData, 16), dutyEnvelopeByte, sweepByte)

        safety += 1
        if safety == 0x50:
            print "Hit the safety break."
            break

# Change this global later.
bubbleBobble = "BubbleBobble.nsf"
if __name__ == "__main__":
    bubbleBobbleHandle = open(bubbleBobble, "rb")
    printSongList(bubbleBobbleHandle)

    # Returns a dictionary in form: [initPointer, initData].
    initData = analyzeInitData(bubbleBobbleHandle, PressingForward)
    print initData
    analyzeSquareOne(bubbleBobbleHandle, initData[0x8205])

    initData = analyzeInitData(bubbleBobbleHandle, PickUpThePace)
    analyzeSquareOne(bubbleBobbleHandle, initData[0x8205])

    print "Success!"

    for note in notes:
        print note
        try:
            notesFile.write(note[0] + ":" + note[1] + ":" + note[2] + "\n")
        except:
            notesFile.write(note[0] + ":" + note[1] + "\n")


    notesFile.close()
